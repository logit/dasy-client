using System;
using System.Collections.Generic;
//using System.IO;

namespace dasyc
{
	public class ArgumentParser
	{
		private string[] Arguments;

		public string FileName {get; set;}
		public string Username {get; set;}
		public string Password {get; set;}
		public string Website {get; set;}
		public string Method {get; set;}
		public string Log {get; set;}
		public ExtraArguments ExtraArgs {get; set;}
		public bool ConsoleOutput {get; set;}

		public ArgumentParser(string[] args)
		{
			this.FileName = "";
			this.Arguments = args;
			this.Username = "";
			this.Password = "";
			this.Website = "";
			this.Method = "";
			this.Log = "";
			this.ExtraArgs = new ExtraArguments();
			this.ConsoleOutput = false;

			this.Parse();
		}
		
		public bool IsOk()
		{
			return (this.Website != "") && (this.Username != "") && (this.Password != "") && (this.Method != "");
		}

		private void Parse()
		{
			for (int i=0; i<this.Arguments.Length; i++ ) {
				string arg = this.Arguments[i];
				if (!arg.StartsWith("-")) {
					continue;
				}

				switch (arg) {
				case "-file":
					this.FileName = this.Arguments[i+1];
					break;
				case "-website":
					this.Website = this.Arguments[i+1];
					break;
				case "-username":
					this.Username = this.Arguments[i+1];
					break;
				case "-password":
					this.Password = this.Arguments[i+1];
					break;
				case "-method":
					this.Method = this.Arguments[i+1];
					break;
				case "-log":
					this.Log = this.Arguments[i+1];
					break;
				case "-arg":
					string key = this.Arguments[i+1].Split('=')[0];
					string val = this.Arguments[i+1].Split('=')[1];
					this.ExtraArgs.Add(key, val);
					break;
				case "-consoleoutput":
					this.ConsoleOutput = true;
					break;
				}
			}
		}
	}
}
