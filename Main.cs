using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dasyc
{
	class MainClass
	{
		private static void Usage() {
			Console.WriteLine("dasyc.exe Universus data sync tool");
			Console.WriteLine("dasyc.exe -website http://www.example.com -username username -password password -method method [optional switches]");
			Console.WriteLine("Mandatory switches are website, username, password and method");
			Console.WriteLine("Optional switches are file, log, arg and consoleoutput");
			Console.WriteLine("");
			Console.WriteLine("For exact usage in your scenario contact http://www.logit.hr support.");
			Console.WriteLine("");
		}
		
		public static void Main(string[] args)
		{
			var parser = new ArgumentParser(args);
			
			if (!parser.IsOk()) {
				Usage();
				return;
			}

			var website = new Website(parser.Website);
			var user = new User(parser.Username, parser.Password);
			var sync = new DataSync(website, user);
			string sync_result = "";
			var log = new StringBuilder();
			
			sync.Init();

			if (sync.Login()) {
				if (parser.Method != "") {
					if (parser.FileName != "") {
						sync_result = sync.SendContent(parser.Method, parser.FileName, parser.ExtraArgs);
						if (parser.ConsoleOutput) {
							Console.WriteLine(sync_result);
						}
						log.AppendLine(sync_result);
					} else {
						sync_result = sync.SendMessage(parser.Method, parser.ExtraArgs);
						if (parser.ConsoleOutput) {
							Console.WriteLine(sync_result);
						}
						log.AppendLine(sync_result);
					}
				} else {
					Console.Error.WriteLine("Method missing.");
				}
				sync.Logout();
			} else {
				Console.Error.WriteLine("Bad username and/or password.");
				return;
			}
			
			if (parser.Log != "") {
				File.WriteAllText(parser.Log, log.ToString());
			}
		}
	}
}
