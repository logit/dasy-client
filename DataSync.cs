using System;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Collections.Generic;

namespace dasyc
{
	public class Website
	{
		public string Root {get; set;}
		public string AdminPath {get {return Root + "/admin/";}}
		public string LoginPath {get {return Root + "/admin/datasync/auth/";}}
		public string LogoutPath {get {return Root + "/admin/logout/";}}
		public string Handler {get {return Root + "/admin/datasync/handler/";}}

		public Website(string website)
		{
			this.Root = website.TrimEnd('/');
		}
	}

	public class User
	{
		public string Username {get; set;}
		public string Password {get; set;}

		public User(string username, string password)
		{
			this.Username = username;
			this.Password = password;
		}
	}
	
	public class ExtraArguments
	{
		private Dictionary<string, string> args;
		public ExtraArguments() {
			this.args = new Dictionary<string, string>();
		}
		
		public void Add(string key, string val) {
			if (!this.args.ContainsKey(key)) {
				this.args.Add(key, val);
			} else {
				this.args[key] = val;
			}
		}
		
		public new string ToString() {
			string s = "&";
			foreach (string key in this.args.Keys) {
				s += string.Format("{0}={1}&", 
					HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(this.args[key]));
			}
			return s.TrimEnd('&');
		}
	}

	public class DataSync
	{
		public string Url;
		private CookieContainer cookies;

		public Website Website;
		public User User;

		public DataSync(Website website, User user)
		{
			this.Website = website;
			this.User = user;
		}

		private HttpWebRequest GetRequest(string url)
		{
			HttpWebRequest result = (HttpWebRequest)WebRequest.Create(url);
			result.ProtocolVersion = HttpVersion.Version11;
			result.CookieContainer = this.cookies;
			result.Timeout = -1; // no timeout
			return result;
		}

		public void Init()
		{
			this.cookies = new CookieContainer();
			HttpWebRequest request = this.GetRequest(this.Website.AdminPath);
			request.CookieContainer = this.cookies;
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			response.Close();
		}

		public bool Login()
		{
			HttpWebRequest request = this.GetRequest(this.Website.LoginPath);
			request.Method = "POST";
			request.ContentType="application/x-www-form-urlencoded";

			string formdata = string.Format("username={0}&password={1}",
				HttpUtility.UrlEncode(this.User.Username),
				HttpUtility.UrlEncode(this.User.Password));

			byte[] postBuffer = Encoding.UTF8.GetBytes(formdata);
			request.ContentLength = postBuffer.Length;

			Stream postBufferStream = request.GetRequestStream();
			postBufferStream.Write(postBuffer, 0, postBuffer.Length);
			postBufferStream.Flush();
			postBufferStream.Close();

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			string result = response.Headers.Get("X-datasync-authenticated");
			response.Close();
			return result == "authenticated";
		}

		public void Logout()
		{
			HttpWebRequest request = this.GetRequest(this.Website.LogoutPath);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			response.Close();
		}

		public string SendContent(string method, string filename, ExtraArguments extraArgs)
		{
			string boundary = "----------" + DateTime.Now.Ticks.ToString("x") + "----------";
			string postHeader = new StringBuilder().
				AppendFormat("--{0}\r\n", boundary).
				AppendFormat("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n",
					             "content", Path.GetFileName(filename)).
				AppendFormat("Content-Type: {0}\r\n\r\n", "application/octet-stream").ToString();
			byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);
			byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

			HttpWebRequest request = this.GetRequest(this.Website.Handler + "?method=" + method + extraArgs.ToString());
			request.ContentType = "multipart/form-data; boundary=" + boundary;
			request.Method = "POST";

			Stream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
			request.ContentLength = postHeaderBytes.Length + fileStream.Length + boundaryBytes.Length;

			Stream requestStream = request.GetRequestStream();

			requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);

			byte[] fileBuffer = new byte[fileStream.Length];
			int bytesRead = 0;
			while ((bytesRead = fileStream.Read(fileBuffer, 0, fileBuffer.Length)) != 0)
			{
				requestStream.Write(fileBuffer, 0, bytesRead);
			}

			requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Encoding encoding = new UTF8Encoding();

			StreamReader responseStream = new StreamReader(response.GetResponseStream(), encoding);
			string result = responseStream.ReadToEnd();

			response.Close();
			responseStream.Close();
			requestStream.Close();
			fileStream.Close();
			fileStream = null;

			return result;
		}

		public string SendMessage(string method, ExtraArguments extraArgs)
		{
			HttpWebRequest request = this.GetRequest(this.Website.Handler + "?method=" + method + extraArgs.ToString());
			request.Method = "POST";

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Encoding encoding = new UTF8Encoding();

			StreamReader responseStream = new StreamReader(response.GetResponseStream(), encoding);
			string result = responseStream.ReadToEnd();

			response.Close();
			responseStream.Close();

			return result;
		}
	}
}
